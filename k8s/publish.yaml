---
apiVersion: apps/v1
kind: Deployment
metadata:
 name: publish
 labels:
  app.kubernetes.io/instance: debian-janitor
spec:
 strategy:
  rollingUpdate:
   maxSurge: 1
   maxUnavailable: 1
  type: RollingUpdate
 replicas: 1
 selector:
  matchLabels:
   app: publish
 template:
  metadata:
   labels:
    app: publish
  spec:
     tolerations:
       - key: cloud.google.com/gke-preemptible
         operator: Equal
         value: "true"
         effect: NoSchedule
     initContainers:
       - name: gpg-key-import
         image: ghcr.io/jelmer/janitor/publish:main
         imagePullPolicy: Always
         command: ['gpg', '--quiet', '--import', '/root/gpg-key/secret.asc']
         volumeMounts:
           - name: gpg-key-volume
             readOnly: true
             mountPath: /root/gpg-key
           - name: gpg-homedir-volume
             mountPath: /root/.gnupg
     containers:
       - name: publish
         image: ghcr.io/jelmer/janitor/publish:main
         imagePullPolicy: Always
         args:
           - "--config=/etc/janitor/janitor.conf"
           - "--external-url=https://janitor.debian.net/"
           - "--reviewed-only"
           - "--require-binary-diff"
           - "--push-limit=500"
           - "--slowstart"
           - "--max-mps-per-bucket=300"
           - "--modify-mp-limit=500"
           - "--interval=3600"
           - "--differ-url=http://differ.default:9920/"
           - "--gcp-logging"
           - "--template-env-path=/etc/janitor/proposal-templates"
           - "--port=9912"
           - "--listen-address=0.0.0.0"
         livenessProbe:
           httpGet:
             path: /health
             port: 9912
           initialDelaySeconds: 3
           periodSeconds: 30
           timeoutSeconds: 30
         readinessProbe:
           httpGet:
             path: /ready
             port: 9912
           initialDelaySeconds: 3
           periodSeconds: 3
           timeoutSeconds: 30
         resources:
           limits:
             cpu: "2"
             memory: "12Gi"
           requests:
             cpu: "1"
             memory: "8Gi"
         ports:
           - containerPort: 9912
         volumeMounts:
           - name: janitor-config-volume
             mountPath: /etc/janitor/
           - name: proposal-templates-volume
             mountPath: /etc/janitor/proposal-templates/
           - name: ssh-key-volume
             mountPath: /root/.ssh/id_rsa
             subPath: id_rsa
             readOnly: true
           - name: ssh-conf-volume
             mountPath: /root/.ssh/config
             subPath: config
           - name: gpg-homedir-volume
             mountPath: /root/.gnupg
           - name: breezy-config-volume
             mountPath: /root/.config/breezy/breezy.conf
             subPath: breezy.conf
           - name: breezy-config-volume
             mountPath: /root/.config/breezy/locations.conf
             subPath: locations.conf
           - name: breezy-secrets-volume
             mountPath: /root/.config/breezy/authentication.conf
             subPath: authentication.conf
           - name: breezy-secrets-volume
             readOnly: true
             mountPath: /root/.config/breezy/gitlab.conf
             subPath: gitlab.conf
           - name: breezy-secrets-volume
             readOnly: true
             mountPath: /root/.config/breezy/github.conf
             subPath: github.conf
         env:
           - name: PGHOST
             value: pooler-janitor-rw.default
           - name: PGPORT
             value: "5432"
           - name: PGDATABASE
             value: "janitor"
           - name: PGUSER
             valueFrom:
               secretKeyRef:
                 name: janitor-pguser-janitor
                 key: user
           - name: PGPASSWORD
             valueFrom:
               secretKeyRef:
                 name: janitor-pguser-janitor
                 key: password
           - name: PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION
             value: python
     volumes:
       - name: janitor-config-volume
         configMap:
           defaultMode: 420
           name: janitor-conf
       - name: breezy-config-volume
         configMap:
           name: breezy-conf
       - name: proposal-templates-volume
         configMap:
           name: proposal-templates
       - name: breezy-secrets-volume
         secret:
           secretName: breezy-secrets
           defaultMode: 256
       - name: ssh-key-volume
         secret:
           secretName: ssh-key
           items:
             - key: ssh-privatekey
               path: id_rsa
           defaultMode: 256
       - name: gpg-key-volume
         secret:
           secretName: gpg-key
       - name: ssh-conf-volume
         configMap:
           name: ssh-conf
           items:
             - key: config
               path: config
           defaultMode: 256
       - name: gpg-homedir-volume
         emptyDir: {}
---
apiVersion: v1
kind: Service
metadata:
 name: publish
 labels:
  app: publish
  app.kubernetes.io/instance: debian-janitor
spec:
 type: ClusterIP
 ports:
  - port: 9912
    name: web
 selector:
  app: publish
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
 name: publish
 labels:
  app: publish
  app.kubernetes.io/instance: debian-janitor
spec:
 selector:
  matchLabels:
   app: publish
 endpoints:
  - port: web

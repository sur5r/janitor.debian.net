default: build-inplace

all: docker-schedule docker-worker k8s docker-site

build-inplace:
	python3 setup.py build_ext --inplace

DOCKER_GCR_NS = eu.gcr.io/debian-janitor/

CNPG_VERSION = 15.1

docker-all: docker-worker docker-site docker-schedule docker-followup docker-schedule

docker-postgres-cnpg: Dockerfile_postgres_cnpg
	buildah build --pull-always -t $(DOCKER_GCR_NS)postgres:$(CNPG_VERSION) -f $< --build-arg=POSTGRES_VERSION=$(CNPG_VERSION) .
	buildah push $(DOCKER_GCR_NS)postgres:$(CNPG_VERSION)

docker-%: Dockerfile_%
	buildah build --pull-always -t $(DOCKER_GCR_NS)$* -f $< .
	buildah push $(DOCKER_GCR_NS)$*:latest

docker-worker: Dockerfile_worker
	docker build --no-cache --pull -t $(DOCKER_GCR_NS)worker -f $< .
	docker push $(DOCKER_GCR_NS)worker:latest

deploy-worker:
	./update-workers.sh

update-worker:
	$(MAKE) docker-worker
	$(MAKE) deploy-worker

deploy-matrix-notify:
	./k8s/kubectl rollout restart deploy matrix-notify

docker-matrix-notify: matrix-notify/Dockerfile $(wildcard matrix-notify/*)
	buildah build --pull-always -t $(DOCKER_GCR_NS)matrix-notify matrix-notify
	buildah push $(DOCKER_GCR_NS)matrix-notify:latest

update-matrix-notify:
	$(MAKE) docker-matrix-notify
	$(MAKE) deploy-matrix-notify

deploy-mastodon-notify:
	./k8s/kubectl rollout restart deploy mastodon-notify

docker-mastodon-notify: mastodon-notify/Dockerfile $(wildcard mastodon-notify/*)
	buildah build --pull-always -t $(DOCKER_GCR_NS)mastodon-notify mastodon-notify
	buildah push $(DOCKER_GCR_NS)mastodon-notify:latest

update-mastodon-notify:
	$(MAKE) docker-mastodon-notify
	$(MAKE) deploy-mastodon-notify

update-schedule:
	$(MAKE) docker-schedule
	$(MAKE) trigger-schedule-codebases
	$(MAKE) trigger-schedule-candidates
	$(MAKE) trigger-policy

deploy-site:
	./k8s/kubectl rollout restart deploy site

update-site:
	$(MAKE) docker-site
	$(MAKE) deploy-site

update-followup:
	$(MAKE) docker-followup
	$(MAKE) deploy-followup

deploy-runner:
	./k8s/kubectl rollout restart deploy runner

deploy-git-store:
	./k8s/kubectl rollout restart deploy git-store

deploy-bzr-store:
	./k8s/kubectl rollout restart deploy bzr-store

deploy-publish:
	./k8s/kubectl rollout restart deploy publish

deploy-followup:
	./k8s/kubectl rollout restart deploy followup

deploy-archive:
	./k8s/kubectl rollout restart statefulset archive

deploy-differ:
	./k8s/kubectl rollout restart statefulset differ

deploy-ognibuild-dep:
	./k8s/kubectl rollout restart deploy ognibuild-dep

trigger-followup-scan:
	-./k8s/kubectl delete job followup-scan
	./k8s/kubectl apply -f k8s/followup-scan.yaml

trigger-schedule: trigger-schedule-codebases trigger-schedule-candidates

trigger-schedule-codebases-%:
	-./k8s/kubectl delete job codebases-$*-manual
	./k8s/kubectl create job --from=cronjob/codebases-$* codebases-$*-manual

trigger-schedule-codebases: $(patsubst %,trigger-schedule-codebases-%,sid bullseye-backports bookworm-backports)

trigger-policy:
	-./k8s/kubectl delete job policy-manual
	./k8s/kubectl create job --from=cronjob/policy policy-manual

trigger-schedule-candidates-%:
	-./k8s/kubectl delete job candidates-$*-manual
	./k8s/kubectl create job --from=cronjob/candidates-$* candidates-$*-manual

SCHEDULE_CAMPAIGNS = bullseye-backports bookworm-backports test-debianize watch fresh-snapshots lintian-fixes mia multi-arch orphan scrub-obsolete vcswatch repology

trigger-schedule-candidates: $(patsubst %,trigger-schedule-candidates-%,$(SCHEDULE_CAMPAIGNS))

deploy-all: deploy-worker deploy-site deploy-runner deploy-git-store deploy-bzr-store deploy-archive deploy-differ trigger-schedule deploy-matrix-notify deploy-mastodon-notify deploy-ognibuild-dep deploy-followup

update-all: update-worker update-site update-followup deploy-runner deploy-git-store deploy-bzr-store deploy-archive deploy-differ update-schedule deploy-matrix-notify deploy-mastodon-notify deploy-ognibuild-dep

k8s:
	$(MAKE) -C k8s

check:: k8s-check

k8s-check:
	$(MAKE) -C k8s check

check:: test

check:: typing

typing:
	mypy debian_janitor schedule

check:: lint

check:: test

test: build-inplace
	PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python py.test tests

style:: lint

lint:: flake8

flake8:
	flake8 debian_janitor/

lint:: djlint

djlint:
	djlint -i H030,H031,H021,J018 --profile=jinja debian_janitor/site/templates

k8s-lint:
	$(MAKE) -C k8s lint

.PHONY: k8s

reformat::
	djlint --quiet --reformat --format-css debian_janitor/site/templates/

check:: validate-policy

validate-policy:
	PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python python3 -m debian_janitor.policy --validate --policy k8s/policy.conf --config k8s/janitor.conf

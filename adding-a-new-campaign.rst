Adding a new campaign
=====================

A campaign is an effort to fix a particular kind of problems. It has
a single associated command to run, and only needs to be run once per package.

Each campaign on the janitor website has its own section, with progress
reports, etc.

For a campaign, you roughly need three things:

* *a codemod script*: run in a version-controlled package directory and
  makes changes to the package
* *a planner*: a script that runs as part of the scheduling job that
  determines what packages the codemod script needs to be run for
  and estimates the "value" (an integer used for prioritization) of each run
* optionally, an extension to the website. Without this, the web site
  will just display some generic pages
* *configuration* to tie all of this together

Create a new codemod script
~~~~~~~~~~~~~~~~~~~~~~~~~~~

At the core of every campaign is a script that can make changes
to a version controlled branch.

This script will be executed in a version controlled checkout of
a source package, and can make changes to the package as it sees fit.
See `this blog post <https://www.jelmer.uk/silver-platter-intro.html>`_ for more
information about creating codemod scripts.

You can test the script independently by running silver-platter, e.g.

``./debian-svp apply --command=myscript --dry-run --diff`` (from a checkout)
or

``./debian-svp run --command=myscript --dry-run --diff package-name``

Add configuration for the campaign
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In janitor.conf, add a section for the campaign. E.g.::

    campaign {
      name: "some-name"
      branch_name: "some-name"
      debian_build {
        archive_description: "Description for use in apt"
        build_suffix: "suf"
      }
    }

In policy.conf, add a default stanza::

    policy {
      campaign {
       name: "some-name"  # This is the name of the campaign
       command: "some-name"  # This is the codemod script to run
       publish { mode: propose }  # Default publishing mode
      }
    }

Add script for finding candidates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add a script that can gather candidates for the new campaign. This script should
be run regularly to find new candidates to schedule, with its JSON output
uploaded to $RUNNER_URL/candidates.

There are some example scripts in schedule/

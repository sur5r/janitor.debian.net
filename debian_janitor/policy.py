#!/usr/bin/python3
# Copyright (C) 2018 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import logging
from typing import TextIO

from google.protobuf import text_format  # type: ignore
from yarl import URL

from . import policy_pb2, _debian_janitor_rs

PolicyConfig = policy_pb2.PolicyConfig


def read_policy(f: TextIO) -> policy_pb2.PolicyConfig:
    return text_format.Parse(f.read(), policy_pb2.PolicyConfig())


read_release_stages = _debian_janitor_rs.read_release_stages
PolicyApplyer = _debian_janitor_rs.PolicyApplier
ExpandedPolicy = _debian_janitor_rs.ExpandedPolicy
publish_policy_name = _debian_janitor_rs.publish_policy_name


def serialize_publish_policy(intended_policy, maintainer_email):
    return {
        'rate_limit_bucket': maintainer_email,
        'per_branch': {
            role: {'mode': mode, 'max_frequency_days': max_frequency_days}
            for role, (mode, max_frequency_days)
            in intended_policy.per_branch.items()}}


async def upload_publish_policy(
        session, publisher_url, name, serialized_publish_policy):
    logging.info("%s -> %r", name, serialized_publish_policy)
    async with session.put(
            publisher_url / 'policy' / name,
            json=serialized_publish_policy,
            raise_for_status=True):
        pass

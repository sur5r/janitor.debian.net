use pyo3::create_exception;
use pyo3::exceptions::{PyRuntimeError, PyValueError};
use pyo3::prelude::*;
use pyo3::types::PyType;
use pyo3_asyncio::tokio as pyo3_tokio;

create_exception!(debian_janitor_rs, InvalidPolicy, PyValueError);

#[pyfunction]
fn read_release_stages<'a>(py: Python<'a>, url: &str) -> PyResult<&'a PyAny> {
    let url = url.to_string();
    pyo3_tokio::future_into_py(py, async move {
        debian_janitor::release::read_release_stages(url.as_str())
            .await
            .map_err(|e| -> PyErr {
                match e {
                    debian_janitor::release::Error::Io(e) => e.into(),
                    debian_janitor::release::Error::Reqwest(e) => {
                        PyRuntimeError::new_err(e.to_string())
                    }
                    debian_janitor::release::Error::SerdeYaml(e) => {
                        PyValueError::new_err(e.to_string())
                    }
                    debian_janitor::release::Error::ChronoParse(e) => {
                        PyValueError::new_err(e.to_string())
                    }
                }
            })
    })
}

#[pyclass]
struct ExpandedPolicy(debian_janitor::policy::ExpandedPolicy);

#[pymethods]
impl ExpandedPolicy {}

#[pyclass(unsendable)]
struct PolicyApplier(debian_janitor::policy::PolicyApplier);

#[pymethods]
impl PolicyApplier {
    #[new]
    fn new() -> Self {
        Self(debian_janitor::policy::PolicyApplier::new())
    }

    #[classmethod]
    fn from_paths(
        _cls: &PyType,
        path: std::path::PathBuf,
        overrides_path: Option<std::path::PathBuf>,
    ) -> PyResult<Self> {
        Ok(Self(
            debian_janitor::policy::PolicyApplier::from_paths(
                path.as_path(),
                overrides_path.as_ref().map(|p| p.as_path()),
            )
            .map_err(|e| -> PyErr {
                match e {
                    debian_janitor::policy::Error::InvalidPolicy(m) => InvalidPolicy::new_err(m),
                    debian_janitor::policy::Error::MaintainerInvalid(m)
                    | debian_janitor::policy::Error::UploaderInvalid(m) => PyValueError::new_err(m),
                }
            })?,
        ))
    }

    fn expand(
        &mut self,
        campaign: &str,
        package_name: &str,
        vcs_url: Option<&str>,
        maintainer: Option<&str>,
        uploaders: Option<Vec<&str>>,
        in_base: Option<bool>,
    ) -> PyResult<ExpandedPolicy> {
        Ok(ExpandedPolicy(
            self.0
                .expand(
                    campaign,
                    package_name,
                    vcs_url,
                    maintainer,
                    uploaders.as_deref(),
                    in_base,
                )
                .map_err(|e| -> PyErr {
                    match e {
                        debian_janitor::policy::Error::InvalidPolicy(m) => {
                            InvalidPolicy::new_err(m)
                        }
                        debian_janitor::policy::Error::MaintainerInvalid(m)
                        | debian_janitor::policy::Error::UploaderInvalid(m) => {
                            PyValueError::new_err(m)
                        }
                    }
                })?,
        ))
    }

    fn known_campaigns(&self) -> PyResult<Vec<String>> {
        Ok(self.0.known_campaigns())
    }

    fn load_release_stages(&mut self) -> PyResult<()> {
        let rt = tokio::runtime::Runtime::new().unwrap();

        rt.block_on(async move {
            self.0.load_release_stages().await.map_err(|e| -> PyErr {
                match e {
                    debian_janitor::release::Error::Io(e) => e.into(),
                    debian_janitor::release::Error::Reqwest(e) => {
                        PyRuntimeError::new_err(e.to_string())
                    }
                    debian_janitor::release::Error::SerdeYaml(e) => {
                        PyValueError::new_err(e.to_string())
                    }
                    debian_janitor::release::Error::ChronoParse(e) => {
                        PyValueError::new_err(e.to_string())
                    }
                }
            })
        })
    }
}

#[pyfunction]
fn publish_policy_name(campaign: &str, package: &str) -> String {
    debian_janitor::policy::publish_policy_name(campaign, package)
}

fn json_to_py(py: Python, v: serde_json::Value) -> PyResult<PyObject> {
    Ok(match v {
        serde_json::Value::Null => py.None(),
        serde_json::Value::Bool(b) => pyo3::types::PyBool::new(py, b).into(),
        serde_json::Value::Number(n) => {
            if let Some(n) = n.as_i64() {
                n.to_object(py)
            } else if let Some(n) = n.as_u64() {
                n.to_object(py)
            } else if let Some(n) = n.as_f64() {
                n.to_object(py)
            } else {
                unreachable!()
            }
        }
        serde_json::Value::String(s) => pyo3::types::PyString::new(py, s.as_str()).into(),
        serde_json::Value::Array(a) => {
            let a: Vec<PyObject> = a
                .into_iter()
                .map(|v| json_to_py(py, v))
                .collect::<PyResult<Vec<_>>>()?;
            pyo3::types::PyList::new(py, a.as_slice()).into()
        }
        serde_json::Value::Object(o) => {
            let ret = pyo3::types::PyDict::new(py);
            for (k, v) in o.into_iter() {
                ret.set_item(k, json_to_py(py, v)?)?;
            }
            ret.into()
        }
    })
}

#[pyclass]
struct RepologyIterator(debian_janitor::repology::RepologyIter);

#[pymethods]
impl RepologyIterator {
    #[new]
    fn new() -> Self {
        Self(debian_janitor::repology::RepologyIter::new())
    }

    fn __iter__(slf: PyRef<Self>) -> PyRef<Self> {
        slf
    }

    fn __next__(&mut self, py: Python) -> PyResult<Option<(String, PyObject)>> {
        if let Some(v) = self.0.next() {
            Ok(Some(
                v.map_err(|e| PyRuntimeError::new_err(e.to_string()))
                    .map(|(k, v)| Ok::<_, PyErr>((k, json_to_py(py, v)?)))??,
            ))
        } else {
            Ok(None)
        }
    }
}

#[pyfunction]
fn load_from_repology() -> PyResult<RepologyIterator> {
    Ok(RepologyIterator(
        debian_janitor::repology::load_from_repology(),
    ))
}

#[pymodule]
fn _debian_janitor_rs(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(read_release_stages, m)?)?;
    m.add_function(wrap_pyfunction!(publish_policy_name, m)?)?;
    m.add_class::<PolicyApplier>()?;
    m.add_class::<ExpandedPolicy>()?;
    m.add_class::<RepologyIterator>()?;
    m.add_function(wrap_pyfunction!(load_from_repology, m)?)?;

    let followupm = PyModule::new(_py, "followup")?;
    followupm.add(
        "PROBLEM_KINDS_TO_IGNORE",
        debian_janitor::followup::PROBLEM_KINDS_TO_IGNORE.to_vec(),
    )?;
    m.add_submodule(followupm)?;
    Ok(())
}

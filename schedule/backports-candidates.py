#!/usr/bin/python3

# Copyright (C) 2018-2020 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import logging

from debian.changelog import Version

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_BACKPORTS = 50
EXISTING_BACKPORT_VALUE = 60


async def iter_backports_candidates(udd, source_release, target_release, packages=None):
    args = [source_release, target_release]
    query = """\
SELECT
  source_series.source AS source,
  source_series.version AS source_version,
  target_series.version AS target_version,
  backport_series.version AS backport_version
FROM sources AS source_series
LEFT JOIN sources AS target_series
    ON source_series.source = target_series.source AND target_series.release = $2
LEFT JOIN sources AS backport_series
    ON source_series.source = backport_series.source
    AND backport_series.release = CONCAT($2, '-backports')
WHERE
  source_series.vcs_url != ''
  AND source_series.release = $1
  AND (target_series.version IS NULL OR source_series.version > target_series.version)
  AND (backport_series.version IS NULL
       OR source_series.version > backport_series.version)
"""
    if packages is not None:
        query += " AND sources_series.source = any($3::text[])"
        args.append(tuple(packages))
    for row in await udd.fetch(query, *args):
        source_uversion = Version(row['source_version'].upstream_version)
        if row.get('backport_version'):
            current_uversion = Version(row['backport_version'].upstream_version)
        elif row.get('target_version'):
            current_uversion = Version(row['target_version'].upstream_version)
        else:
            current_uversion = None
        if current_uversion is not None and source_uversion == current_uversion:
            continue
        print(f'# target currently has {row["target_version"]}')
        print(f'# backports currently has {row["backport_version"]}')
        candidate = Candidate()
        # TODO(jelmer): This should really be source_release, but we don't
        # fetch testing yet
        candidate.distribution = "sid"
        candidate.package = row['source']
        value = DEFAULT_VALUE_BACKPORTS
        if row["backport_version"] is not None:
            value += EXISTING_BACKPORT_VALUE
        candidate.value = value
        candidate.campaign = f"{target_release}-backports"
        candidate.context = str(row['source_version'])
        candidate.extra_arg.append(f"--version={row['source_version']}")
        yield candidate


async def main():
    import argparse

    import asyncpg
    from distro_info import DebianDistroInfo

    debian_info = DebianDistroInfo()

    parser = argparse.ArgumentParser(prog="backports-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--source-release", type=str, default=debian_info.testing())
    parser.add_argument("--target-release", type=str, default=debian_info.stable())
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(
            format='%(message)s',
            level=(logging.DEBUG if args.debug else logging.INFO))

    if not args.target_release:
        parser.print_usage()
        parser.exit()

    udd = await asyncpg.connect(args.udd_url)
    await udd.set_type_codec(
        "debversion", format="text", encoder=str, decoder=Version)
    async for candidate in iter_backports_candidates(
            udd, args.source_release, args.target_release, args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())

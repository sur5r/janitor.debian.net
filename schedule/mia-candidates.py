#!/usr/bin/python3

import logging
from typing import Any

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_MIA = 70

# TODO(jelmer): Import these constants from drop-mia-uploaders
MIA_EMAIL = "mia@qa.debian.org"
MIA_TEAMMAINT_USERTAG = "mia-teammaint"


async def iter_mia_candidates(udd, release, packages=None):
    query = """\
SELECT source, id from bugs
WHERE
  id IN (select id from bugs_usertags where email = $1 and tag = $2) AND
  status = 'pending'
"""
    args: list[Any] = [MIA_EMAIL, MIA_TEAMMAINT_USERTAG]
    if packages is not None:
        query += " AND sources.source = any($3::text[])"
        args.append(tuple(packages))
    for row in await udd.fetch(query, *args):
        candidate = Candidate()
        candidate.distribution = release
        candidate.package = row[0]
        candidate.campaign = "mia"
        candidate.context = str(row[1])
        candidate.value = DEFAULT_VALUE_MIA
        yield candidate


async def main():
    import argparse

    import asyncpg

    parser = argparse.ArgumentParser(prog="mia-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    udd = await asyncpg.connect(args.udd_url)
    async for candidate in iter_mia_candidates(udd, "sid", args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())

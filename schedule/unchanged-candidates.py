#!/usr/bin/python3

# Copyright (C) 2018-2020 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import logging

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_UNCHANGED = 1


async def iter_unchanged_candidates(udd, release, packages=None):
    args = [release]
    query = """\
SELECT DISTINCT ON (sources.source) \
sources.source FROM sources WHERE \
sources.vcs_url != '' AND \
sources.release = $1
"""
    if packages is not None:
        query += " AND sources.source = any($2::text[])"
        args.append(tuple(packages))
    for row in await udd.fetch(query, *args):
        candidate = Candidate()
        candidate.distribution = release
        candidate.package = row[0]
        candidate.value = DEFAULT_VALUE_UNCHANGED
        candidate.campaign = "unchanged"
        yield candidate


async def main():
    import argparse

    import asyncpg

    parser = argparse.ArgumentParser(prog="unchanged-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument("--release", type=str, default="sid")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    udd = await asyncpg.connect(args.udd_url)
    async for candidate in iter_unchanged_candidates(
            udd, args.release, args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())

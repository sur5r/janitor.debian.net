#!/usr/bin/python3

from setuptools import setup
from setuptools_protobuf import Protobuf
from setuptools_rust import Binding, RustExtension, RustBin

setup(protobufs=[
    Protobuf('debian_janitor/policy.proto', mypy=True),
    Protobuf('debian_janitor/candidates.proto', mypy=True),
    Protobuf('debian_janitor/package_metadata.proto', mypy=True),
    Protobuf('debian_janitor/package_overrides.proto', mypy=True),
    Protobuf('debian_janitor/upstream_project.proto', mypy=True),
], rust_extensions=[
        RustBin(
            "analyze-branch-unknown", "Cargo.toml"),
        RustBin(
            "refresh-policy", "Cargo.toml"),
        RustExtension(
            "debian_janitor._debian_janitor_rs",
            "debian-janitor-py/Cargo.toml", binding=Binding.PyO3),
])

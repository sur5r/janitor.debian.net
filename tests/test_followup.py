#!/usr/bin/python
# Copyright (C) 2022 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA


from debian_janitor.followup import create_app
from janitor.config import Config


async def create_client(
        aiohttp_client, db, publisher_url, policy_applier, requirement_resolvers):
    return await aiohttp_client(await create_app(
        db=db, publisher_url=publisher_url, policy_applier=policy_applier, config=Config(),
        requirement_resolvers=requirement_resolvers))


async def test_health(aiohttp_client):
    client = await create_client(
        aiohttp_client, publisher_url="http://localhost:9912/",
        policy_applier=None, db=None,
        requirement_resolvers=None)

    resp = await client.get("/health")
    assert resp.status == 200
    text = await resp.text()
    assert text == "ok"


async def test_ready(aiohttp_client):
    client = await create_client(
        aiohttp_client, publisher_url="http://localhost:9912/",
        policy_applier=None, db=None,
        requirement_resolvers=None)

    resp = await client.get("/ready")
    assert resp.status == 200
    text = await resp.text()
    assert text == "ok"


async def test_process_unknown(aiohttp_client, db):
     client = await create_client(
        aiohttp_client, publisher_url="http://localhost:9912/",
        policy_applier=None, db=db,
        requirement_resolvers=None)

     resp = await client.post("/process/some-nonexistant-id")
     assert resp.status == 404


async def test_scan(aiohttp_client, db):
     client = await create_client(
        aiohttp_client, publisher_url="http://localhost:9912/",
        policy_applier=None, db=db,
        requirement_resolvers=None)

     resp = await client.post("/scan", json={})
     assert resp.status == 200

use std::io::{self, Write};
use std::path::PathBuf;
use std::process::{Command, Stdio};
use tempfile::TempDir;
use trivialdb::{Flags, Tdb};

const BANNED_URLS: &[&str] = &["https://salsa.debian.org/haskell-team/DHG_packages.git"];

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut check = Tdb::open(
        PathBuf::from("checked.tdb").as_path(),
        None,
        Flags::empty(),
        libc::O_RDWR | libc::O_CREAT,
    )
    .unwrap();

    let response = reqwest::blocking::get(
        "https://janitor.debian.net/api/result-codes/upstream-branch-unknown",
    )?;
    let runs: Vec<serde_json::Value> = response.json()?;
    let mut packages = Vec::new();

    for entry in runs {
        let package = entry["package"].as_str().unwrap().to_string();
        let vcs_type = entry["vcs_type"].as_str().unwrap().to_string();
        let url = entry["branch_url"].as_str().unwrap().to_string();
        packages.push((package, vcs_type, url));
    }

    for (package, _vcs_type, url) in packages {
        if check.exists(package.as_bytes()) {
            continue;
        }
        if BANNED_URLS.contains(&url.as_str()) {
            continue;
        }
        println!("package: {} ({})", package, url);

        let temp_dir = TempDir::new()?;
        let temp_dir_path = temp_dir.path();

        Command::new("brz")
            .arg("clone")
            .arg(url)
            .arg(temp_dir_path.to_str().unwrap())
            .output()?;

        let grep_output = Command::new("brz")
            .args([
                "grep",
                "-Xdebian/*",
                "(github.com|bitbucket.org|gitlab.com|gitlab|sf.net)",
            ])
            .current_dir(temp_dir_path)
            .output()?;

        let mut output = Vec::new();
        output.extend_from_slice(&grep_output.stdout);

        let grep_output2 = Command::new("brz")
            .args([
                "grep",
                "-Xdebian/*",
                "(CVS|Subversion|svn|SVN|Git|Bazaar|bzr|fossil)",
            ])
            .current_dir(temp_dir_path)
            .output()?;
        output.extend_from_slice(&grep_output2.stdout);

        io::stdout().write_all(&output)?;
        io::stdout().flush()?;

        let result = if !output.is_empty() {
            Command::new("/bin/bash")
                .current_dir(temp_dir_path)
                .stdin(Stdio::inherit())
                .stdout(Stdio::inherit())
                .stderr(Stdio::inherit())
                .output()?;
            "yes"
        } else {
            "nothing"
        };

        check.store(package.as_bytes(), result.as_bytes(), None)?;
    }

    Ok(())
}

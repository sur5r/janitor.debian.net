use clap::{Arg, Command};
use debian_janitor::policy::{sync_publish_policy, validate_policy, PolicyApplier};
use janitor::config::read_file;
use janitor::state;

#[tokio::main]
async fn main() {
    let matches = Command::new("expand-policy")
        .arg(
            Arg::new("config")
                .long("config")
                .value_name("FILE")
                .default_value("janitor.conf")
                .help("Path to configuration."),
        )
        .arg(
            Arg::new("policy")
                .long("policy")
                .value_name("FILE")
                .default_value("policy.conf")
                .help("Path to policy configuration."),
        )
        .arg(
            Arg::new("package-overrides")
                .long("package-overrides")
                .value_name("FILE")
                .help("Package overrides."),
        )
        .arg(
            Arg::new("campaign")
                .value_name("CAMPAIGN")
                .required(true)
                .help("Campaign name."),
        )
        .arg(
            Arg::new("package")
                .value_name("PACKAGE")
                .required(true)
                .help("Package name."),
        )
        .arg(
            Arg::new("maintainer")
                .value_name("MAINTAINER")
                .help("Maintainer."),
        )
        .arg(Arg::new("vcs-url").value_name("VCS_URL").help("VCS URL."))
        .arg(
            Arg::new("in-base")
                .long("in-base")
                .action(clap::ArgAction::SetTrue)
                .help("In base."),
        )
        .get_matches();

    let config_path: &String = matches.get_one("config").unwrap();
    let policy_path: &String = matches.get_one("policy").unwrap();
    let package_overrides: Option<&String> = matches.get_one("package-overrides");
    let package: Option<&String> = matches.get_one("package");
    let campaign: Option<&String> = matches.get_one("campaign");
    let vcs_url: Option<&String> = matches.get_one("vcs-url");
    let maintainer: Option<&String> = matches.get_one("maintainer");
    let in_base = matches.get_flag("in-base");
    let uploaders: Option<&[&str]> = None;

    env_logger::init();

    let mut policy_applier = PolicyApplier::from_paths(
        std::path::Path::new(policy_path),
        package_overrides.as_ref().map(std::path::Path::new),
    )
    .unwrap();
    policy_applier.load_release_stages().await.unwrap();

    let expanded_policy = policy_applier
        .expand(
            campaign.unwrap(),
            package.unwrap(),
            vcs_url.map(|s| s.as_str()),
            maintainer.map(|s| s.as_str()),
            uploaders,
            Some(in_base),
        )
        .unwrap();

    println!("{:?}", expanded_policy);
}

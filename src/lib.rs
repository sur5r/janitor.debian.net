mod generated {
    include!(concat!(env!("OUT_DIR"), "/generated/mod.rs"));
}

pub mod followup;
pub mod overrides;
pub mod package_metadata;
pub mod packages;
pub mod policy;
pub mod release;
pub mod repology;

use sqlx::postgres::PgConnection;

#[derive(sqlx::FromRow)]
pub struct Package {
    pub name: String,
    pub vcs_url: Option<String>,
    pub maintainer_email: Option<String>,
    pub uploader_emails: Vec<String>,
    pub in_base: Option<bool>,
}

pub async fn iter_packages(
    conn: &mut PgConnection,
    package: Option<&str>,
) -> Result<Vec<Package>, sqlx::Error> {
    let mut query = "SELECT
        name,
        vcs_url,
        maintainer_email,
        uploader_emails,
        in_base
    FROM
        package
    WHERE
        NOT removed"
        .to_owned();

    if let Some(package_name) = package {
        query += " AND name = $1";

        sqlx::query_as::<_, Package>(&query)
            .bind(package_name)
            .fetch_all(conn)
            .await
    } else {
        sqlx::query_as::<_, Package>(&query).fetch_all(conn).await
    }
}

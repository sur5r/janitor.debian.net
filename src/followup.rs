use crate::policy::ExpandedPolicy;
use janitor::config::Config;
use sqlx::postgres::PgConnection;
use url::Url;

pub const PROBLEM_KINDS_TO_IGNORE: &[&str] = &[
    "hosted-on-alioth",
    "unexpected-local-upstream-changes",
    "unsupported-vcs",
    "unsupported-vcs-svn",
    "unsupported-vcs-hg",
    "worker-failure",
    "401-unauthorized",
    "too-many-requests",
];

pub enum Error {}

pub trait Action {
    fn schedule(
        &self,
        conn: &PgConnection,
        config: &Config,
        publisher_url: Option<&Url>,
        expanded_policy: &ExpandedPolicy,
    ) -> Result<i32, Error>;

    fn json(&self) -> serde_json::Value;
}

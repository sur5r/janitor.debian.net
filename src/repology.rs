use log::debug;
use std::collections::HashMap;
use std::thread;
use std::time::Duration;

use reqwest::blocking::Client;

pub fn load_from_repology() -> RepologyIter {
    RepologyIter::new()
}

pub struct RepologyIter {
    client: Client,
    last_project: Option<String>,
    pending: Vec<(String, serde_json::Value)>,
}

impl RepologyIter {
    pub fn new() -> Self {
        Self {
            client: Client::new(),
            last_project: None,
            pending: Vec::new(),
        }
    }
}

impl Iterator for RepologyIter {
    type Item = Result<(String, serde_json::Value), reqwest::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(item) = self.pending.pop() {
            return Some(Ok(item));
        }
        let url = format!(
            "https://repology.org/api/v1/projects/{}",
            self.last_project.as_deref().unwrap_or("")
        );
        debug!("Retrieving {}", url);
        let response = self
            .client
            .get(&url)
            .header("Accept", "application/json")
            .send()
            .ok()?;

        let data: HashMap<String, serde_json::Value> = response.json().ok()?;
        let data_len = data.len();

        if data_len == 0 {
            return None;
        }

        let last_project_key = data.keys().max().unwrap().to_string();
        self.pending.extend(data.into_iter());

        self.last_project = Some(format!("{}/", last_project_key));
        thread::sleep(Duration::from_secs(1));

        Some(Ok(self.pending.pop().unwrap()))
    }
}
